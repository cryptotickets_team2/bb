var http = require('http');
var url = require('url');
var querystring = require('querystring');
var fs = require('fs');
var coloured = require('./coloured');

var design_top = fs.readFileSync('./design_top.html');
var design_bottom = fs.readFileSync('./design_bottom.html');
var default_content = fs.readFileSync('./default_content.html');

var srv;

function httpsend(body) {
	srv.end(design_top + body + design_bottom);
}

http.createServer(function(req, res) {
	var curpage = url.parse(req.url).pathname;

	srv = res;

	res.writeHead(200, {'Content-Type': 'text/html'});

	if(curpage === '/buy') {
		coloured.sell(querystring.parse(url.parse(req.url).query).data, httpsend);
	}else if(curpage === '/qrtest') {
		httpsend("<img src='http://qrcoder.ru/code/?"+encodeURIComponent(JSON.stringify(coloured.genkey()))+"&4&0' />");
	}else if(curpage === '/issue_asset') {
		content = coloured.issue_asset(httpsend);
	}else if(curpage === '/issue') {
		coloured.issue(querystring.parse(url.parse(req.url).query).data, httpsend);
	}else{
		httpsend(default_content);
	}

}).listen(8888, '127.0.0.1');

console.log('Server running at http://127.0.0.1:8888/');
