(function() {
	var request = require('request');
	var bitcoin = require('bitcoinjs-lib');
	var FEE = 5000;

	var key = {"addr":"mxByVTJDwBZJx3QHAhy2cxsBjtmzedyUE7","wif":"KwdhwKtEsbun8FdaDMT9JoRG7Eif2KgVfMXYBax2NXE8ufodpDMy"};

	// aux
	function postToApi(api_endpoint, json_data, callback) {
		console.log(api_endpoint+': ', JSON.stringify(json_data));
		request.post({
			url: 'http://testnet.api.coloredcoins.org:80/v3/'+api_endpoint,
			headers: {'Content-Type': 'application/json'},
			form: json_data
		},
		function(error, response, body) {
			if(error) {
				return callback(error);
			}
			if(typeof body === 'string') {
				body = JSON.parse(body)
			}
			console.log('Status: ', response.statusCode);
			console.log('Body: ', JSON.stringify(body));
			return callback(null, body);
		});
	};

	function getFromApi(api_endpoint, param, callback) {
		console.log('Get from:'+api_endpoint+'/'+param);
		request.get('http://testnet.api.coloredcoins.org:80/v3/'+api_endpoint+'/'+param, function (error, response, body) {
			if(error) {
				return callback(error);
			}
			if(typeof body === 'string') {
				body = JSON.parse(body)
			}
			console.log('Status:', response.statusCode);
			console.log('Body:', body);
			return callback(null, body);
		});
	};

	function signTx(unsignedTx, wif) {
		var privateKey = bitcoin.ECKey.fromWIF(wif);
		var tx = bitcoin.Transaction.fromHex(unsignedTx);
		var insLength = tx.ins.length;
		for (var i = 0; i < insLength; i++) {
			tx.sign(i, privateKey);
		}
		return tx.toHex();
	}

	function _genkey() {
		return bitcoin.ECKey.makeRandom();
	}

	function genkey() {
		var _newkey = _genkey();
		var newkey = { "addr": _newkey.pub.getAddress(bitcoin.networks.testnet).toString(), "wif": _newkey.toWIF() };
		return newkey;
	}

	module.exports.genkey = genkey;

	function fund(addr) {

	}

	// API
	module.exports.sell = function(data, callback) {
		var newkey = genkey();
		var sendasset = {
			'from': [key.addr],
			'fee': FEE,
			'to': [{
				'address': newkey.addr,
				'amount': 1,
				'assetId': data
			}]
		};
		postToApi('sendasset', sendasset, function(err, body) {
			if(err) {
				console.log('error: ', err);
			}else{
				console.log('sended');
				var signed = signTx(body.txHex, key.wif);
				console.log('signed: \n'+signed);
				var trans = { 'txHex': signed };
				postToApi('broadcast', trans, function(err, body) {
					if(err) {
						console.log('error: ', err);
					}else{
						console.log('Ok transfer\n');

						// fund
						getFromApi('addressinfo', key.addr, function(err, body) {
							if(err) {
								console.log('error: ', err);
							}else{
								console.log('here we are');
								var c;
								var tx = 0;
								var val;
								for(c = 0; c < body.utxos.length; c++) {
									if(!body.utxos[c].used && body.utxos[c].value > 10000) {
										tx = body.utxos[c].txid;
										val = body.utxos[c].value;
										break;
									}
								}
								if(tx != 0) {
									console.log('tx found :)');
									var _bc_key = bitcoin.ECKey.fromWIF(key.wif);
									var _bc_tx = new bitcoin.TransactionBuilder();
									_bc_tx.addInput(tx, 1);
									_bc_tx.addOutput(newkey.addr, FEE);
									_bc_tx.addOutput(key.addr, val - (FEE * 2));
									_bc_tx.sign(0, _bc_key);
									console.log(_bc_tx.build().toHex());

									// push it
									var trans2 = { 'txHex': _bc_tx.build().toHex() };
									postToApi('broadcast', trans2, function(err, body){
										if(err) {
											console.log('error: ', JSON.stringify(err));
										}else{
											console.log('Ok transfer\n');
											console.log(JSON.stringify(body));
											callback("<img src='http://qrcoder.ru/code/?"+encodeURIComponent(JSON.stringify(newkey))+"&4&0' />");
										}
									});
								}
							}
						});
					}
				});
			}
		});
	}


	module.exports.issue = function(data, callback) {
		postToApi('issue', JSON.parse(data), function(err, body) {
			if(err) {
				console.log('error: ', err);
			}else{
				var assetId = body.assetId;
				var signed = signTx(body.txHex, key.wif);

				var trans = { 'txHex': signed };
				postToApi('broadcast', trans, function(err, body) {
					if(err) {
						console.log('error: ', err);
					}else{
						console.log('Ok\n');
						callback(assetId);
					}
				});
			}
		});
	}

	module.exports.issue_asset = function(callback) {
		var asset = {
			'issueAddress': key.addr,
			'amount': 10,
			'fee': FEE
		};

		callback("<textarea name='asset' id='asset' rows='16' cols='64'>"+JSON.stringify(asset)+"</textarea><button onclick='window.location = \"/issue?data=\"+document.getElementById(\"asset\").value\'>issue</button>");
	}
}());
