var bitcoin = require('bitcoinjs-lib');
var request = require('request');
var fs = require('fs');


// fs

var d = JSON.parse(fs.readFileSync('./addr.txt'));
console.log(d);
// return;

var address = d.addr;
var wif = d.wif;

console.log('address: '+address+'\nwif: '+wif+'\n');
//var key = bitcoin.ECPair.makeRandom({network: bitcoin.networks.testnet});
// var key = bitcoin.ECKey.makeRandom();
// //var address = key.getAddress().toString();
// var address = key.pub.getAddress(bitcoin.networks.testnet).toString();
// var wif = key.toWIF();
console.log('new TESTNET address: ['+address+']');
console.log('Private Key of new address (WIF format): ['+wif+']');

// aux
function postToApi(api_endpoint, json_data, callback) {
    console.log(api_endpoint+': ', JSON.stringify(json_data));
    request.post({
        url: 'http://testnet.api.coloredcoins.org:80/v3/'+api_endpoint,
        headers: {'Content-Type': 'application/json'},
        form: json_data
    },
    function (error, response, body) {
        if (error) {
            return callback(error);
        }
        if (typeof body === 'string') {
            body = JSON.parse(body)
        }
        console.log('Status: ', response.statusCode);
        console.log('Body: ', JSON.stringify(body));
        return callback(null, body);
    });
};


// fs
// fs.writeFile('./addr.txt', JSON.stringify({addr: address, wif: wif}))		;


function signTx (unsignedTx, wif) {
    var privateKey = bitcoin.ECKey.fromWIF(wif)
    var tx = bitcoin.Transaction.fromHex(unsignedTx)
    var insLength = tx.ins.length
    for (var i = 0; i < insLength; i++) {
        tx.sign(i, privateKey)
    }
    return tx.toHex()
}


// try
var asset = {
    'issueAddress': address,
    'amount': 10,
    'fee': 5000
};
postToApi('issue', asset, function(err, body){
    if (err) {
        console.log('error: ', err);
    }

    var assetId = body.assetId;
    var signed = signTx(body.txHex, wif);
    console.log('signed: \n'+signed);

		var trans = { 'txHex': signed };
		postToApi('broadcast', trans, function(err, body){
				if (err) {
						console.log('error: ', err);
				}else{
					console.log('Ok\n');
				}


				var send_asset = {
						'from': [address],
						'fee': 5000,
						'to': [{
							'address': 'muCVj8gaLDLSzw4kH6nb4qW3eNrZSaUUUw',
							'amount': 3,
							'assetId': assetId
						}]
				};

				postToApi('sendasset', send_asset, function(err, body){
						if (err) {
								console.log('error: ', err);
						}else{
							console.log('sended');
							var signed = signTx(body.txHex, wif);
							console.log('signed: \n'+signed);
							var trans = { 'txHex': signed };
							postToApi('broadcast', trans, function(err, body){
									if (err) {
											console.log('error: ', err);
									}else{
										console.log('Ok transfer\n');
									}
							});
						}
				});

		});

});
